// importing timesheet_model
import { Timesheet_Data } from "../model.js";

/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
function emp_ts_handler(req, res, next) {
  const { emp_id } = req;

  // finding timesheet data of the specific employee by using employee id.
  Timesheet_Data.find({ emp_id: emp_id }, (err, data) => {
    if (data) {
      // declaring an empty array to push the timesheet_data of each day as object
      const emp_ts_details = []

      // iterating over each timesheet_obj_data
      data.map((each_ts_obj) => {
        const { meeting_hrs, training_hrs, appraisal_hrs, task_hrs, hiring_hrs, ts_sheet, date } = each_ts_obj;

        let total_working_hours = 0;
        // summing up all the working hours of each day and pushing them into array
        if (typeof meeting_hrs !== "undefined") {
          total_working_hours += meeting_hrs;
          if (typeof task_hrs !== "undefined") {
            total_working_hours += task_hrs;
            if (typeof training_hrs !== "undefined") {
              total_working_hours += training_hrs;
              if (typeof appraisal_hrs !== "undefined") {
                total_working_hours += appraisal_hrs;
                if (typeof hiring_hrs !== "undefined") {
                  total_working_hours += hiring_hrs;
                }
              }
            }
          }
        }

        // parsing date object to string YYYY-MM-DD date fromatt
        var dateString = new Date(
          date.getTime() - date.getTimezoneOffset() * 60000
        )
          .toISOString()
          .split("T")[0];
        
        var today = new Date()
        const this_month = "0" + (today.getMonth() + 1)
        
        // formatting the date to DD-MM-YYYY date formatt
        const formatted_date = dateString.split("-")[2] + "-" + dateString.split("-")[1] + "-" + dateString.split("-")[0]

        const ts_obj = {date: formatted_date, total_time: total_working_hours, ts_status: ts_sheet}

        const month = dateString.split("-")[1]

        // checking the month and pushing the timesheet details of this month to get monthly summary
        if (month === this_month) {
          emp_ts_details.push(ts_obj)
        }
       
      });
      // sending this month timesheet summary as response
      res.status(200).send(emp_ts_details);
    } else {
      // sending status_code 404 and status_message if data is not found
      res.status(404).send({ status_message: "Error in finding data" });
    }
  });
}

export { emp_ts_handler };
