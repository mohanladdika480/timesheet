// importing TimeSheet_Data model
import { Timesheet_Data } from "../model.js";

/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
function ts_request_handler(req, res, next) {
  let check_emp_id = [];
  let ts_requests = [];
  Timesheet_Data.find({}, (err, data) => {
    if (err) {
      res
        .status(400)
        .send({ status_message: "Error in finding timesheet requests" });
    }
    if (data.length !== 0) {
      data.map((each_ts_obj) => {
        const { emp_id, name } = each_ts_obj;

        // checking if the employee has multiple ts requests, if not appending data to array
        if (!check_emp_id.includes(emp_id)) {
          const emp_obj = { emp_id: emp_id, name: name };
          check_emp_id.push(emp_id);
          ts_requests.push(emp_obj);
        }
      });
      res.status(200).send(ts_requests);
    } else {
      res
        .status(404)
        .send({ status_message: "Timesheet requests not found or upto date" });
    }
  });
}

export { ts_request_handler };
