// importing Employee_Data Model
import {Employee_Data} from "../model.js"

/**
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
function emp_profile_handler(req, res, next) {
    const {emp_id} = req

    //finding the data of employee by using emp_id
    Employee_Data.findOne({emp_id:emp_id}, (err, data)=> {
        if (data) {
            let count = 0
            for (let key in data) {
                count += 1
            }
            // checking whether the data is empty or not
            if (count!==0) {
                // if data retrieved is not empty sending employee details in response
                const {emp_id, email, name, role, mobile_no, aadhar_no} = data

                const dob = date_formatter(data.dob)

                const date_of_join = date_formatter(data.date_of_join)

                const emp_details = {emp_id, email, name, dob, date_of_join, role, mobile_no, aadhar_no}
                res.status(200).send(emp_details)
            }
            else {
                // if the data is not retrieved then send status code 404 and error message as response
                res.status(404).send({status_message: "Employee Details not Found"})
            }
        }
        // if there is error sends error code and message as response
        else {
            res.status(400).send({status_message: "Error in Finding Employee Details"})
        }
    })

}


function date_formatter(date) {
    var dateString = new Date(
        date.getTime() - date.getTimezoneOffset() * 60000
      )
        .toISOString()
        .split("T")[0];
        const formatted_date = dateString.split("-")[2] + "-" + dateString.split("-")[1] + "-" + dateString.split("-")[0]
    return formatted_date;
}

export {emp_profile_handler}