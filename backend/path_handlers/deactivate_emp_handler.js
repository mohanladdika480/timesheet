// importing Employee_data model
import { Employee_Data } from "../model.js";

/**
 *
 * @param {*} req emp_id as body params
 * @param {*} res if the params allready exists in database sends error status_code and status_message
 * @param {*} next its an express middleware
 * sends status code-404 and messege :Error in finding user by id if  emp_id does not exist
 * sends sta
 * 
 */
function deactivate_emp_handler(req, res, next) {
  const { emp_id, emp_status } = req.body;
  //fectching data based on employee_id
  Employee_Data.findOne({ emp_id: emp_id }, (err, data) => {
    if (err) {
      //sending error response
      res
        .status(404)
        .send({ status_message: "Error in finding user by employee id" });
    } else {
      //updating status based on emp_id
      if (data) {
        Employee_Data.updateOne(
          { emp_id: emp_id },
          { $set: { emp_status: emp_status } }
        )
          .then(() => {
            //sending response as successful
            res
              .status(200)
              .send({ status_message: "Employee status updated Successfully" });
          })
          .catch(() => {
            //sending unable to do response
            res
              .status(304)
              .send({ status_message: "Unable to update database" });
          });
      } else {
        //sending emp_id does not found response
        res.status(404).send({ status_message: "Employee Id doesn't exist" });
      }
    }
  });
}


//exporting function
export { deactivate_emp_handler };
