// import bcrypt
import bcrypt from "bcrypt";

// importing Employee_data model
import { Employee_Data } from "../model.js";

import {jwt_token_generator} from "../utils/jwt_token/jwt_token.js"
/**
 * This function checks if the user exists and the secret key matches. If its a
 * match, the password is updated to new password.
 *
 * @param {*} req email,password,secret_key from body which are used for forgot functionality
 * @param {*} res sends statuscode and status_message as response for successful login '200'
 * @param {*} next it is an express middleware
 * @returns
 *
 * if the employee doesnt fetched by email
 * sends status code-404 and status_messege as error in finding user by email as response
 *
 * if the employee is fetched but secret key does not match
 * sends status code-401 and status_messege as secret_key is not the right one
 *
 * if unable to update, then
 * sends status code-304 and status_message as unable to update the database
 *
 * if secrety_key matches, then password is updated
 *
 */
async function forgot_handler(req, res, next) {
  // accessing email and pasword from body
  const { email, password, secret_key } = req.body;

  const hashed_password = await bcrypt.hash(password, 10);

  // Checking whether the employee email exists or not
  Employee_Data.findOne({ email: email }, (err, data) => {
    if (err) {
      res
        .status(404)
        .send({ status_message: "Error in finding user by email" });
    } else {
      if (!data) {
        res
          .status(404)
          .send({ status_message: "Error in finding user by email" });
      } else {
        if (secret_key != data.secret_key) {
          res
            .status(401)
            .send({ status_message: "Secret key is not the right one" });
        } else {
                const emp_id = data.emp_id
                    const role = data.role
                    const name = data.name
                    const jwt_token = jwt_token_generator(emp_id, role, name)
                    if (jwt_token !== "undefined") {
          Employee_Data.updateOne(
            { email: email },
            { $set: { password: password } }
          )
            .then(() => {
              res.status(200).send(JSON.stringify({jwt_token:jwt_token, role:role}));
            })
            .catch(() => {
              res.status(304).send({ status_message: "Unable to update database" });
            });
          }
          else {
            res.status(400).send({status:"Password updation unsuccessfull"})
        }
        }
      }
    }
  });
}

export { forgot_handler };
