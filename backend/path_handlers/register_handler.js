// importing bcrypt
import bcrypt from "bcrypt";

// importing Employee_data model
import { Employee_Data } from "../model.js";

/**
 * This function is used for creating the employee profile
 * @param {*} req all the params that are used for creation of employee profile
 * @param {*} res sends status code and status message as response
 * @param {*} next it is an express middleware
 *
 * sends statuscode as 201 and profile created suceessfull as response
 * 
 * if profile not created sends statuscode as 412 and profile not created as response
 *
 */
async function register_handler(req, res, next) {
  // accepting the params from body and storing them in variable keys
  const { name, emp_id, email, role, mobile_no, aadhar_no } = req.body;
  const { dob, gender, date_of_join, emp_status, secret_key, password  } = req.body;

  // generate salt to hash password
  const salt = await bcrypt.genSalt(10);
  // now we set user password to hashed password
  const hashed_password = await bcrypt.hash(req.body.password, salt);

  // storing all the required data for employee profile creation in a variable
  const profile_data = {
    name,
    emp_id,
    email,
    role,
    password,
    mobile_no,
    aadhar_no,
    dob,
    gender,
    date_of_join,
    emp_status,
    secret_key, 
    password
  };

  const employee_data = new Employee_Data(profile_data);
                  // saving the employee_data into the database
                  employee_data.save()
                  .then(() => {
                    // if the profile is created successfully sends succesfull response
                    res
                      .status(201)
                      .send({ status_message: "Profile created successfully" })
                  })
                  .catch(()=> {
                    res.status(412).send("Profile not created")
                  });

  
}

export { register_handler };
