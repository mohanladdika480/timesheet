// import bcrypt
import bcrypt from "bcrypt"

// importing Employee_data model 
import {Employee_Data} from "../model.js"

// importing jwt_token_genrator
import {jwt_token_generator} from "../utils/jwt_token/jwt_token.js"


/**
 * This function is used to verify whether the user exists or not.
 * If the user exists then checks the log_in params for login purpose
 * @param {*} req email,password from body which are used for user login 
 * @param {*} res sends statuscode and status_message as response for successfull login '200' 
 * @param {*} next it is an express middleware
 * @returns
 * 
 * if the employee doesnt fetched by email 
 * sends status code-404 and status_messege as error in finding user by email as response
 * if password matches
 * if jwt created
 * sends status code-200 and jwt token  as response
 * 
 * if jwt not created
 * sends status code-400 and login unsuccesfull as response
 * 
 * if password doesnot matches
 * sends status code-404 and password doesnt match as response
 * 
 * 
 * if the user doesnot exists 
 * sends status code-404 and email doesnt exist as response
 * 
 */
const login_handler = async(req, res, next) => {
    
    // accessing email and pasword from body
    const {email, password} = req.body;
    const user = await Employee_Data.findOne({email: email})

    
    
    // Checking whether the employee email exists or not
    Employee_Data.findOne({email:email}, (err, data)=> {
        if (err) {
            res.status(404).send({status_message: "Error in finding user by email"})
        }
        else {
            // if user exists checks for password
            if (data) {
                
                if (data.emp_status === "Active") {
                // if password matches calls the jwt_token_generator()
                
                if (password === data.password) {
                  
                    const emp_id = data.emp_id
                    const role = data.role
                    const name = data.name
                    const jwt_token = jwt_token_generator(emp_id, role, name)
                    // if jwt_token is generated then login successful
                    if (jwt_token !== "undefined") {
                        Employee_Data.updateOne({email:email},
                                        {$set: {jwt_token:jwt_token}

                        }).then(()=>{
                            res.status(200).send(JSON.stringify({jwt_token:jwt_token, role:role}))
                        })
                    }
                    // if jwt_token is not generated then sends response as login unsuccessful
                    else {
                        res.status(400).send({status:"log in unsuccessfull"})
                    }
                }
                // if the password doesn't matches sends 404 as response
                else {
                    res.status(404).send({status:"invalid password"})
                }
                }
                else {
                    res.status(200).send({status_message: "You must be an Employee to access this feature"})
                }
            }
            // if user doesn't exists sends 404 as response
            else {
                res.status(404).send({status:"Email doesn't exists"})
            }
        }
    })
    
}

export {login_handler}