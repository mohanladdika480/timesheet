import { Timesheet_Data } from "../model.js";


/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
function timesheet_handler(req, res, next) {
  const emp_id = req.emp_id;
  const role = req.role;
  const name = req.name;
  console.log(emp_id, role, name)
  const {
    meeting_hrs,
    meeting_desc,
    training_hrs,
    training_desc,
    appraisal_hrs,
  } = req.body;
  const { appraisal_desc, task_hrs, task_desc, hiring_hrs, hiring_desc, date } =
    req.body;

  const ts_data = {
    emp_id,
    name,
    meeting_hrs,
    meeting_desc,
    training_hrs,
    training_desc,
    appraisal_hrs,
    appraisal_desc,
    task_hrs,
    task_desc,
    hiring_hrs,
    hiring_desc,
    date,
  };
  const timesheet = new Timesheet_Data(ts_data)
  timesheet.save().then(()=> {
      res.status(200).send({status_message: "Timesheet Filled Successfully"})
  })
  .catch(()=> {
      res.status(400).send({status_message: "Timesheet Request Failed"})
  })
}

export { timesheet_handler };
