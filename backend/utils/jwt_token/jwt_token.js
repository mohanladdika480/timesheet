// importing jsonwebtoken
import jwt from "jsonwebtoken";

/**
 * jwt_token_generator() is used for generating jwt_token when the user logins
 * @param {*} emp_id which is used as part of payload
 * @param {*} role which is used as a part of the payload
 * @returns jwt_token which is generated
 */
function jwt_token_generator(emp_id, role, name) {
  // accepting emp_id as params and storing it in payload as object
  var payload = {
    emp_id: emp_id,
    role: role,
    name: name
  };

  // generates jwt token
  const jwt_Token = jwt.sign(payload, "MY_SECRET_TOKEN");
  return jwt_Token;
}

/**
 * jwt_validator_employee() is used for validating whether the user is logged in or not as an employee role
 * @param {*} req authorization_token  from headers
 * @param {*} res 
 * @param {*} next its an express midlleware
 */
function jwt_validator_employee(req, res, next) {
  jwt_validator_helper(req, res, next, ["Employee", "HR", "Admin"])
}

/**
 * jwt_validator_hr() is used for validating whether the user is logged in or not as an HR role
 * @param {*} req authorization_token  from headers
 * @param {*} res 
 * @param {*} next its an express midlleware
 */
 function jwt_validator_hr(req, res, next) {
  jwt_validator_helper(req, res, next, ["HR", "Admin"])
}

/**
 * jwt_validator_admin() is used for validating whether the user is logged in or not as an Admin role
 * @param {*} req authorization_token  from headers
 * @param {*} res 
 * @param {*} next its an express midlleware
 */
 function jwt_validator_admin(req, res, next) {
  jwt_validator_helper(req, res, next, ["Admin"])
}

/**
 * jwt_validator_helper is a heler function to check for roles in input list
 * @param {*} req authorization_token  from headers
 * @param {*} res 
 * @param {*} next its an express midlleware
 */
 function jwt_validator_helper(req, res, next, array_of_roles) {
  const authentication_token = req.headers.authorization;

  if (authentication_token !== undefined) {
    const jwt_token = authentication_token.split(" ")[1];

    // verifying jwt_token and accessing payload from the jwt_token
    jwt.verify(jwt_token, "MY_SECRET_TOKEN", (error, payload) => {
      if (error) {
        res.status(401).send("Invalid Jwt Token");
        }
      else {
        let count = 0;
        for (let role of array_of_roles) {
          count += 1
        }
        if (count !== 0) {
          req.emp_id = payload.emp_id;
          req.role = payload.role;
          req.name = payload.name;
          next();
        }
        else {
                res.status(401).send("Unauthorized role, your role is " + payload.role + " and not in " + array_of_roles)
             }
      } 
    })
    }
    }
       

export { jwt_token_generator, jwt_validator_employee, jwt_validator_hr, jwt_validator_admin };
