import { _validate_timesheet_param } from "./user_param_validation_helper.js";

/**
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
function validate_timesheet_params(req, res, next) {
  const {
    meeting_hrs,
    meeting_desc,
    training_hrs,
    training_desc,
    appraisal_hrs,
  } = req.body;
  const { appraisal_desc, task_hrs, task_desc, hiring_hrs, hiring_desc, date } =
    req.body;

  let total_working_hours = 0;
  // summing up all the working hours of each day and pushing them into array
  if (typeof meeting_hrs !== "undefined") {
    total_working_hours += meeting_hrs;
    if (typeof task_hrs !== "undefined") {
      total_working_hours += task_hrs;
      if (typeof training_hrs !== "undefined") {
        total_working_hours += training_hrs;
        if (typeof appraisal_hrs !== "undefined") {
          total_working_hours += appraisal_hrs;
          if (typeof hiring_hrs !== "undefined") {
            total_working_hours += hiring_hrs;
          }
        }
      }
    }
  }

	// if total working hours are less than 8 or greater than 16 then sends error code and message as response
  if (total_working_hours < 8 || total_working_hours > 16) {
    res.status(406).send("Total Working hours should be in between 8 and 16");
  }

	// validating the meeting description 
  const meeting_desc_response = _validate_timesheet_param(meeting_desc);

	// if meeting description is not in valid formatt sends error code and message as response
	if (meeting_desc_response.status_code !== 200) {
		res.status(meeting_desc_response.status_code).send(meeting_desc_response.status_message)
	}

	// validating the training description
	const training_desc_response = _validate_timesheet_param(training_desc)

	// if training description is not in valid formatt sends error code and message as response
	if (training_desc_response.status_code !== 200) {
		res.status(training_desc_response.status_code).send(training_desc_response.status_message)
	}

	// validating the appraisal description
	const appraisal_desc_response = _validate_timesheet_param(appraisal_desc)

	// if appraisal description is not in valid formatt sends error code and message as response
	if (appraisal_desc_response.status_code !== 200) {
		res.status(appraisal_desc_response.status_code).send(appraisal_desc_response.status_message)
	}

	// validating the task description
	const task_desc_response = _validate_timesheet_param(task_desc) 

	// if task description is not in valid formatt sends error code and message as response
	if (task_desc_response.status_code !== 200) {
		res.status(task_desc_response.status_code).send(task_desc_response.status_message)
	}

	// validating the hiring description
	const hiring_desc_response = _validate_timesheet_param(hiring_desc)

	// if hring description is not in valid formatt sends error code and message as response
	if (hiring_desc_response !== 200) {
		res.status(hiring_desc_response.status_code).send(hiring_desc_response.status_message)
	}
	else {
		next();
	}
}

export { validate_timesheet_params };
