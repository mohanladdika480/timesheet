/**
 * This function validates the email for specific conditions 
 * @param {*} email which is used for validation purpose 
 * @returns 
 */
function _validate_email(email) {
	// validating email parameter recieved
	if (typeof(email) === "undefined") {
		return {status_code:400, status_message: "email is a mandatory parameter"}
	} else if (typeof(email) !== 'string') {
		return {status_code:400, status_message: "email should be a string"}
	} else {
		if (!email.toLowerCase().match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
				return {status_code:400, status_message: "email is supposed to be in a valid email format"}
		} 
	}
	return {status_code:200}
}


/**
 * 
 * @param {*} password 
 * @returns 
 */
function _validate_password(password) {
	if (typeof(password) === "undefined") {
		return {status_code:400, status_message: "password is a mandatory parameter"}
	} else if (typeof(password) !== 'string') {
		return {status_code:400, status_message: "password is supposed to be a string"}
	} else {
		if (!password.toLowerCase().match(
			// length 6-16, at least a number, and at least a special character
      /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/)) {
		return {status_code:400, status_message: "password is supposed to contain at least 1 number, at least 1 special character and length should be betwen 6 and 16"}
		} 
	}
	return {status_code:200}
}


/**
 * 
 * @param {*} name 
 * @returns 
 */
function _validate_name(name) {
    if (typeof(name) === "undefined") {
        return {status_code:406, status_message:"Name is a mandatory parameter"}
    }
    else if (typeof(name) !== "string") {
        return {status_code:406, status_message: "Name should be a string"}
    }
    else if (name.length <5 || name.length >20) {
        return {status_code:406, status_message: "Name should have minimum 5 chars and maximum 20"}
    }
    else if (name.includes("  ")) {
        return {status_code:406, status_message: "Name has consecutive spaces"}
    }
    else if  (!/^[a-zA-Z ]+$/.test(name)) {
        return {status_code:406, status_message: "Name should be in only alphabets"}
    }
    else {
        return {status_code:200}
    }
}

/**
 * 
 * @param {*} emp_id 
 * @returns 
 */
function _validate_employee_id(emp_id) {
    if (typeof(emp_id) === "undefined") {
        return {status_code:406, status_message:"Employee Id is a mandatory parameter"}
    }
    else if (typeof(emp_id) !== "string") {
        return {status_code:406, status_message:"Employee Id should be a string"}
    }
    else if (emp_id.length!==6) {
        return {status_code:406, status_message: "Employee Id must have six chars"}
    }
    else if (!/^[a-zA-Z0-9]*$/.test(emp_id)) {
        return {status_code:406, status_message: "Employee Id should be alpha-numeric only"}
    }
    else {
        return {status_code:200}
    }
}

/**
 * 
 * @param {*} role 
 * @returns 
 */
function _validate_role(role) {
    if (typeof(role) === "undefined") {
        return {status_code:200}
    }
    else if (typeof(role) !== "string") {
        return {status_code:406, status_message: "Role should be a string"}
    }
    else {
        const list_of_roles = ["Employee", "HR" , "Admin"]
        if (!list_of_roles.includes(role)) {
            return {status_code:406, status_message: "Role should be Employee or HR or Admin"}
        }
    }
    return {status_code:200}
}

/**
 * 
 * @param {*} mobile_no 
 * @returns 
 */
function _validate_mobile_no(mobile_no) {
    if (typeof(mobile_no) === "undefined") {
        return {status_code:406, status_message: "Mobile Number is mandatory"}
    }
    else if (!Number.isInteger(mobile_no) || mobile_no<0) {
        return {status_code:406, status_message: "Mobile Number is supposed to be in valid formatt"}
    }
    else {
        const mobile_no_str = String(mobile_no)
        if (mobile_no_str.length !== 10) {
            return {status_code:406, status_message: "Mobile Number is supposed to have 10 digits"}
        }
    }
    return {status_code:200}
}

/**
 * 
 * @param {*} aadhar_no 
 * @returns 
 */
function _validate_aadhar_no(aadhar_no) {
    if (typeof(aadhar_no) === "undefined") {
        return {status_code:406, status_message: "Aadhar Number is mandatory"}
    }
    else if (!Number.isInteger(aadhar_no) || aadhar_no<0) {
        return {status_code:406, status_message: "Aadhar Number is supposed to be in valid formatt"}
    }
    else {
        const aadhar_no_str = String(aadhar_no)
        if (aadhar_no_str.length !== 12) {
            return {status_code:406, status_message: "Aadhar Number is supposed to have 12 digits"}
        }
    }
    return {status_code:200}
}

/**
 * 
 * @param {*} date 
 * @returns
 */
function _validate_date(date) {
    if (typeof(date) === "undefined") {
        return {status_code:406, status_message: " is a mandatory parameter"}
    }
    else if (!/^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(date)) {
        return {status_code:406, status_message: " should be in valid date formatt"}
    }
    else {
        return {status_code: 200}
    }
   
}

/**
 * 
 * @param {*} gender 
 * @returns 
 */
function _validate_gender(gender) {
    if (typeof(gender) === "undefined") {
        return {status_code:200}
    }
    else if (typeof(gender) !== "string") {
        return {status_code:406, status_message: "Gender should be a string"}
    }
    else {
        const list_of_gender = ["Male", "Female" , "Others"]
        if (!list_of_gender.includes(gender)) {
            return {status_code:406, status_message: "Gender should be Male or Female or others"}
        }
    }
    return {status_code:200}
}

/**
 * 
 * @param {*} emp_status 
 * @returns 
 */
function _validate_emp_status(emp_status) {
    if (typeof(emp_status) === "undefined") {
        return {status_code:200}
    }
    else if (typeof(emp_status) !== "string") {
        return {status_code:406, status_message: "Employee Status should be a string"}
    }
    else {
        const list_of_emp_status = ["Active", "Fired", "Resigned", "OnLeave", "WFH"]
        if (!list_of_emp_status.includes(emp_status)) {
            return {status_code:406, status_message: "Employee status should be Active or Firedcor Resigned or OnLeave or WFH"}
        }
    }
    return {status_code:200}
}

/**
 * 
 * @param {*} secret_key 
 * @returns
 */
function _validate_secret_key(secret_key) {
    if (typeof(secret_key) === "undefined") {
		return {status_code:400, status_message: "Secret key is a mandatory parameter"}
    }
    else if (typeof(secret_key) !== "string") {
        return {status_code:406, status_message: "Secret Key is supposed to be a string"}
    }
    else if (secret_key.length<8 || secret_key.length>30) {
        return {status_code:406, status_message: "Secret Key should be minimum 8 chars or maximum 30 chars"}
    }
    return {status_code:200}
}

/**
 * 
 * @param {*} ts_desc 
 * @returns 
 */
function _validate_timesheet_param(ts_desc) {
    if (typeof(ts_desc) === "undefined") {
        return {status_code:200}
    }
    else if (typeof(ts_desc) !== "string") {
        return {status_code:406, status_message: "Description should be a string"}
    }
    else if (!/^[a-zA-Z ]{10,30}$/.test(ts_desc)) {
        return {status_code:406, status_message: "Description should be a valid formatt and the length should be betwee 10 to 30 chars"}
    }
    else {
        return {status_code: 200}
    }
}

export {_validate_email, _validate_password, _validate_name, _validate_employee_id, _validate_role, _validate_mobile_no, _validate_aadhar_no, _validate_date, _validate_gender, _validate_emp_status, _validate_secret_key, _validate_timesheet_param}