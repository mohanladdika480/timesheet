// importing validation helper functions
import {_validate_email, _validate_password, _validate_name, _validate_employee_id, _validate_role, _validate_mobile_no, _validate_aadhar_no, _validate_date, _validate_gender, _validate_emp_status, _validate_secret_key} from "./user_param_validation_helper.js"

/**
 * This is a middleware function to validate the params of /login call
 * @param req  name,email,mobile_no,dob,gender,aadhar_no,password,emp_id,date_of_join from body
 * @param res sends statuscode and status_message as response for unsuccessfull login '400'
 * @param next it is an express middle ware
 * 
 * if name is empty 
 * sends status code-406 and messege  name should not be empty in response
 *
 * if name is not a string 
 * sends statuscode-400 and  messege name should be a string in response
 * 
 * if name contains any special char 
 * sends statuscode-400 and  messege name should not contain special chars in response
 * 
 * if email is empty
 * sends status code-406 and status as email is a mandatory parameter in response
 * 
 * if email is not a string
 * sends status code-406 and status as email should be a string in reponse
 * 
 * if emails does not follow email format
 * sends status code-400 and  status as email should follow valid format in response 
 * 
 * if password is not given 
 * sends status code-400 and status as password is a mandatory parameter in response
 * 
 * if password is not a string
 * sends status code-400 and status as password should be a string in reponse
 * 
 * if emails does not include special chars,digits and doesnot have a specific length
 * sends status code-400 and  status as email should follow valid format in response 
 * 
 * if mobile_no is empty
 * sends status code-406 and status as mobile_no is a mandatory parameter in response
 * 
 * if mobile_no is not a number
 * sends status code-400 and status as mobile_no should be a number in reponse
 * 
 * if mobile_no does not have ten digits
 * sends status code-406 and status as mobile_no should be a 10 digit number in reponse
 * 
 * if dob is empty
 * sends status code-406 and status as dob is a mandatory parameter in response
 * 
 * if dob doesnt follow date format 
 * sends status code-406 and status as dobshould be a valid format in response
 * 
 * if gender is empty
 * sends status code-406 and status as dob is a gender parameter in response
 * 
 * if gender is not in the specified list of male,female,and others
 * sends status code-406 and status as dob is a gender parameter in response
 * 
 *
 */

function validate_register_params(req, res, next) {
    // accepting the params from body and storing them in variable keys
  const { name, emp_id, email, role, mobile_no, aadhar_no } = req.body;
  const { dob, gender, date_of_join, emp_status, secret_key, password } = req.body;

    // validating employee id parameter
    const emp_id_response = _validate_employee_id(emp_id)

    if (emp_id_response.status_code !== 200) {
        res.status(emp_id_response.status_code).send(emp_id_response.status_message)
    }
    
    // validating email parameter recieved
	const email_response = _validate_email(email)

	if (email_response.status_code !== 200) {
		res.status(email_response.status_code).send(email_response.status_message)
	}

	// validating the password parameter recieved
	const password_response = _validate_password(password)

	if (password_response.status_code !== 200) {
		res.status(password_response.status_code).send(password_response.status_message)
	}

    // validating name parameter recieved
    const name_response = _validate_name(name)

    if (name_response.status_code !== 200) {
        res.status(name_response.status_code).send(name_response.status_message)
    }


    // validating role parameter
    const role_response = _validate_role(role)

    if (role_response.status_code !== 200) {
        res.status(role_response.status_code).send(role_response.status_message)
    }

    // validating mobile number
    const mobile_no_response = _validate_mobile_no(mobile_no)

    if (mobile_no_response.status_code !== 200) {
        res.status(mobile_no_response.status_code).send(mobile_no_response.status_message)
    }

    // validating aadhar number 
    const aadhar_no_response = _validate_aadhar_no(aadhar_no)

    if (aadhar_no_response.status_code !== 200) {
        res.status(aadhar_no_response.status_code).send(aadhar_no_response.status_message)
    }

    // validating dob parameter
    const dob_response = _validate_date(dob) 

    if (dob_response.status_code !== 200) {
        const status_message = { status_message: ("Date of Birth" + dob_response.status_message)}
        res.status(dob_response.status_code).send(status_message)
    }

    // validating gender parameter 
    const gender_response = _validate_gender(gender) 

    if (gender_response.status_code !== 200) {
        res.status(gender_response.status_code).send(gender_response.status_message)
    }

    // validating date of join parameter
    const date_of_join_response = _validate_date(date_of_join)

    if (date_of_join_response.status_code !== 200) {
        const status_message = { status_message: ("Date of Join" + date_of_join_response.status_message)}
        res.status(date_of_join_response.status_code).send(status_message)
    }

    // validating employee status parameter
    const emp_status_response = _validate_emp_status(emp_status)

    if (emp_status_response.status_code !== 200) {
        res.status(emp_status_response.status_code).send(emp_status_response.status_message)
    }

    // validating secret key parameter
    const secret_key_response = _validate_secret_key(secret_key)

    if (secret_key_response.status_code !== 200) {
        res.status(secret_key_response.status_code).send(secret_key_response.status_message)
    }
    else {
        next();
    }
}

export {validate_register_params}
