import {_validate_email, _validate_password} from "./user_param_validation_helper.js"


/**
 * This is a middleware function to validate the params of /login call
 * @param req email, password from req.body
 * @param res sends statuscode and status_message as response for unsuccessfull login '400'
 * @param next it is an express middle ware after passing all validations it goes to login_hanlder
 * 
 * if email is not given 
 * sends status code-400 and status as email is a mandatory parameter as response
 * 
 * if email is not a string
 * sends status code-400 and status as email should be a string as reponse
 * 
 * if emails does not follow email format
 * sends status code-400 and  status as email should follow valid format as response 
 * 
 * if password is not given 
 * sends status code-400 and status as password is a mandatory parameter as response
 * 
 * if password is not a string
 * sends status code-400 and status as password should be a string as reponse
 * 
 * if emails does not include special chars,digits and doesnot have a specific length
 * sends status code-400 and  status as email should follow valid format as response 
 *  
*/
 function validate_login_params(req, res, next) {
	const { email, password } = req.body;

	// validating email parameter recieved
	const email_response = _validate_email(email)

	if (email_response.status_code !== 200) {
		res.status(email_response.status_code).send(email_response.status_message)
	}

	// validating the password parameter recieved
	const password_response = _validate_password(password)

	if (password_response.status_code !== 200) {
		res.status(password_response.status_code).send(password_response.status_message)
	}
	else {
		next();
	}
} 



export {validate_login_params}