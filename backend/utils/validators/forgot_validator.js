import { _validate_email, _validate_password, _validate_secret_key } from "./user_param_validation_helper.js"

function validate_forgot_params(req, res, next) {
    // accepting the params from body and storing them in variable keys
  const { email, secret_key, password } = req.body;

    // validating email parameter recieved
	const email_response = _validate_email(email)

	if (email_response.status_code !== 200) {
		res.status(email_response.status_code).send(email_response.status_message)
	}

	// validating the password parameter recieved
	const password_response = _validate_password(password)

	if (password_response.status_code !== 200) {
		res.status(password_response.status_code).send(password_response.status_message)
	}

    // validating secret key parameter
    const secret_key_response = _validate_secret_key(secret_key)

    if (secret_key_response.status_code !== 200) {
        res.status(secret_key_response.status_code).send(secret_key_response.status_message)
    }
    else {
        next();
    }
}

export {validate_forgot_params}