// importing express
import express from "express";
// importing body-parser
import bodyParser from "body-parser";


// creating an instance of express
const app = express();
// returns middleware that only parses json
app.use(bodyParser.json());
// returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

import cors from "cors";

app.use(cors({origin:"*"}))

//importing internal dependencies
import { login_handler } from "./path_handlers/login_handler.js"

import {validate_login_params} from "./utils/validators/login_validator.js"

import {register_handler} from "./path_handlers/register_handler.js"

import {forgot_handler} from "./path_handlers/forgot_handler.js"

import {register_db_validator} from "./db_utils/register_db_validator.js"

import {validate_register_params} from "./utils/validators/register_validator.js"

import {validate_forgot_params} from "./utils/validators/forgot_validator.js"

import {jwt_validator_employee, jwt_validator_hr, jwt_validator_admin } from "./utils/jwt_token/jwt_token.js"

import {timesheet_handler} from "./path_handlers/timesheet_hanlder.js"

import {deactivate_emp_handler} from "./path_handlers/deactivate_emp_handler.js"

import { emp_ts_handler } from "./path_handlers/employee_ts_handler.js";

import { validate_timesheet_params } from "./utils/validators/timesheet_validator.js";

import { emp_profile_handler } from "./path_handlers/emp_profile_handler.js";

import { ts_request_handler } from "./path_handlers/ts_request_handler.js";



//-----post calls ---------------------------------//

app.post("/login", validate_login_params, login_handler)

app.post("/register", jwt_validator_admin, validate_register_params, register_db_validator, register_handler)

app.post("/timesheet", jwt_validator_employee,validate_timesheet_params, timesheet_handler)


//-----get calls ---------------------------------------//

// app.get("/emp_requests/:emp_id",emp_request_handler)

app.get("/requests", jwt_validator_hr, ts_request_handler)

app.get("/monthly_summary", jwt_validator_employee, emp_ts_handler)

app.get("/employee_data", jwt_validator_employee, emp_profile_handler)

//------ put calls --------------------------------------//

app.put("/deactivate_employee",jwt_validator_admin, deactivate_emp_handler)

app.put("/forgot", validate_forgot_params, forgot_handler)

// JWT validation should be role specific Employe ( e, h , a), HR ( h , a ), Admin ( a )
// jwt_validator_all, jwt_validator_hr, jwt_validator_admin

// Listens to the connections on the specified host and port.
app.listen(3002,()=>{
    console.log("The server is up and running")
})
