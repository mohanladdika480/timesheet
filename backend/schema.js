//importing mongoose
import mongoose from "mongoose";

// employee_schema 
const employee_schema = new mongoose.Schema ({
    emp_id :      {type:String, required:true, unique:true},
    name :        {type:String, required:true},
    email:        {type:String, required:true, unique:true},
    role:         {type:String, default:"Employee", enum:["HR", "Admin", "Employee"]},
    password:     {type:String, required:true},
    mobile_no:    {type:Number, required:true},
    aadhar_no:    {type:Number, required:true, unique:true},
    dob:          {type:Date, required:true},
    gender:       {type:String, enum:["Male","Female", "Others"]},
    date_of_join: {type:Date, required:true},
	jwt_token:    {type:String},
	secret_key:   {type:String, required:true},
	emp_status:   {type:String, default:"Active", enum:["Active", "Deactivated"]}
})


// timesheet schema
const timesheet_schema = new mongoose.Schema ({
	  	emp_id:         {type:String, required:true},
		name:           {type:String, required:true},
		meeting_hrs:    {type:Number, min:0, max:24},
		metting_desc:   {type:String},
		training_hrs:   {type:Number, min:0, max:24},
		training_desc:  {type:String},
		appraisal_hrs:  {type:Number, min:0, max:24},
		appraisal_desc: {type:String},
		task_hrs:       {type:Number, min:0, max:24},
		task_desc: 		{type:String},
		hiring_hrs:     {type:Number, min:0, max:24},
		hiring_desc:    {type:String},
		ts_sheet:		{type:String, default:"Pending", enum:["Pending", "Approved", "Rejected"]},
		date:           {type:Date, required:true}
 })


// exporting schemas
export {employee_schema,timesheet_schema}