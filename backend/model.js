// importing mongoose
import mongoose from "mongoose";

// importing schemas
import { employee_schema, timesheet_schema } from "./schema.js";

// connecting to database
mongoose.connect("mongodb+srv://mohanladdika480:86FOpxtPjhdcac4t@cluster0.zudh5.mongodb.net/myFirstDatabase?retryWrites=true&w=majority");

// creating Employee_data model for employee_schema
const Employee_Data = mongoose.model("Employee_Data", employee_schema);

// creating Timesheet_data model for timesheet_schema
const Timesheet_Data = mongoose.model("Timesheet_Data", timesheet_schema)

//exporting models
export {Employee_Data, Timesheet_Data}