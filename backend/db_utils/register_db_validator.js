//importing internal dependencies
// importing Employee_Data model
import { Employee_Data } from "../model.js";

/**
 * register_db_handler() is used for unique email, emp_id, aadhar_no
 * @param {*} req email, emp_id, aadhar_no as body params and checks for uniqueness
 * @param {*} res if the params allready exists in database sends error status_code and status_message
 * @param {*} next it is middleware if all cteria of this function is met it will allows to go for next function
 *  sends status_code-409 and status_msg-AAdhar must be uinque as response
 *  sends status_code-409 and status_msg-Email must be uinque as response
 *  sends status_code-409 and status_msg-Employee_id must be uinque as response
 */

function register_db_validator(req, res, next) {
  const { email, emp_id, aadhar_no } = req.body;

  // checking whether the employee id is unique or not
  Employee_Data.findOne({ emp_id: `${emp_id}` }, (error, data) => {
    if (data) {
      if (typeof data.emp_id !== "undefined") {
        // checking whether the email is unique or not
        if (data.email !== email) {
            // checking whether the aadhra number is unique or not
            if (data.aadhar_no !== aadhar_no) {
                next();
            }
            else {
                res.status(409).send({status_message: "Aadhar Number must be Unique"})
            }
        }
        else{
            res.status(409).send({status_message: "Email must be Unique"})
        }
      } else {
        res.status(409).send({ status_message: "Employee Id must be Unique" });
      }
    }
  });
  next();
}

//exporting function
export { register_db_validator };
