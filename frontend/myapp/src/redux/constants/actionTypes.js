const actionTypes = {
  SET_LOGIN_DATA: "SET_LOGIN_DATA",
  SET_PROFILE_DATA: "SET_PROFILE_DATA",
  SET_TIMESHEET_DATA: "SET_TIMESHEET_DATA",
  SET_INITIAL_STATE_ON_LOGOUT: "SET_INITIAL_STATE_ON_LOGOUT",
};

export default actionTypes;
