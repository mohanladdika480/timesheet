import { combineReducers } from "redux";

import {
  employeeLoginReducer,
  employeeProfileReducer,
  employeeTimesheetReducer,
  setInitialStateReducer,
} from "./allReducers";

const reducers = combineReducers({
  employeeLogin: employeeLoginReducer,
  employeeProfile: employeeProfileReducer,
  employeeTimesheet: employeeTimesheetReducer,
});

export default reducers;
