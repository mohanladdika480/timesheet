import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import AdminDisablePage from "./compons/AdminDisablePage/AdminDisablePage";
import HrApprovalPage from "./compons/HrApprovalPage/HrApprovalPage";
import HrTimeSheetPage from "./compons/TImeSheetPage/TimeSheetPage";
import Employmonthly from "./compons/EmployMonthlyPage/employmonthly";
import ForgotPage from "./compons/ForgotPage/ForgotPage";
import Loginpage from "./compons/login/Loginpage";
import Createprofile from "./compons/createprofile/createprofile";
import EmployeeProfile from "./compons/EmployeeProfilePage/EmployeeProfile";
import HrTimeSheetRequests from "./compons/HrTimeSheetRequests/HrTimeSheetRequests";
import "./App.css";
import ProtectedRoute from "./compons/ProtectedRoute/ProtectedRoute.js";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/login">
            <Loginpage />
          </Route>
          <Route exact path="/forgot_password">
            <ForgotPage />
          </Route>
          <ProtectedRoute exact path="/deactivate_employee">
            <AdminDisablePage />
          </ProtectedRoute>
          <ProtectedRoute exact path="/hr_ts_approval">
            <HrApprovalPage />
          </ProtectedRoute>
          <ProtectedRoute exact path="/timesheet">
            <HrTimeSheetPage />
          </ProtectedRoute>
          <ProtectedRoute exact path="/employee_monthly_ts">
            <Employmonthly />
          </ProtectedRoute>
          <ProtectedRoute exact path="/create_profile">
            <Createprofile />
          </ProtectedRoute>
          <ProtectedRoute exact path="/profile">
            <EmployeeProfile />
          </ProtectedRoute>
          <ProtectedRoute exact path="/hr_ts_requests">
            <HrTimeSheetRequests />
          </ProtectedRoute>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
