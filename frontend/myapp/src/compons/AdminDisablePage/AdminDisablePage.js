import "./AdminDisablePage.css";
import React, { Component } from "react";

class AdminDisablePage extends Component {
  render() {
    return (
      <>
        <div className="top_bar">
          <div className="d-flex flex-row">
            <div className="top_bar_data">
              <h1 className="cmp_name">Company</h1>
              <p className="cmp_detail">Work Space</p>
            </div>
            <div className="top_bar_icon">
              <div className="d-flex flex-row">
                <div>
                  <p className="top_bar_quote">
                    Success isn't always about greatness. <br />
                    It's about consistency. Consistent hard work leads to
                    success. Greatness will come.
                  </p>
                </div>
                <div>
                  <img
                    src="https://cdn.pixabay.com/photo/2018/03/10/12/00/teamwork-3213924__340.jpg"
                    className="top_bar_image"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="d-flex flex-row">
          <div className="side_bar">
            <div className="profile_data">
              <img
                src="https://1.bp.blogspot.com/-Muc21xaQjYg/XluhYO6f6vI/AAAAAAAAPJw/T51WnNCctz4Vxn_9REzJuftkznfuRGKUgCLcBGAsYHQ/s1600/whatsapp%2Bprofile%2Bpic%2B%25281%2529.jpg"
                className="profile"
              />
              <h1 className="profile_name">Harish Kumar</h1>
            </div>
            <ul>
              <li>
                <i class="fas fa-user-alt"></i>Profile
              </li>
              <li>
                <i className="fas fa-business-time" />
                Time Sheet
              </li>
              <li>
                <i className="fas fa-envelope-open-text" />
                Time Sheet Requests
              </li>
              <li>
                <i className="fas fa-address-card" /> Create Profile{" "}
              </li>
              <li className="hr_active">
                <i className="fas fa-user-alt-slash" /> Deactivate Profile{" "}
              </li>
              <li>
                <i className="fas fa-sign-out-alt" /> Log Out{" "}
              </li>
            </ul>
          </div>
          <div className="main_sec">
            <h1 className="welcome_msg"> Welcome, Harish Kumar </h1>
            <h1 className="admin_emp_create"> Deactivate Employee Profile </h1>
            <form className="emp_form">
              <h1 className="emp_form_desc"> Deactivate Employee Profile </h1>
              <div>
                <label for="emp_cmp_mail" className="label_emp">
                  Employee Id
                </label>
                <input type="text" name="emp_cmp_mail" />
                <br />
                <label for="emp_password" className="label_emp">
                  Status
                </label>
                <input type="text" name="emp_password" maxLength={12} />
              </div>
            </form>
            <button className="emp_ts_button"> Back </button>
            <button className="emp_ts_button"> Submit </button>
          </div>
        </div>
        <div className="bottom_bar">
          <h1 className="bottom_bar_heading">Company</h1>
          <p className="bottom_bar_desc">
            {" "}
            Copyright @2022, Company Software Pvt Ltd. All rights reserved.{" "}
          </p>
        </div>
      </>
    );
  }
}

export default AdminDisablePage;
