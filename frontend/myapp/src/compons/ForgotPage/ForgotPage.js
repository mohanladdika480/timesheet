import "./ForgotPage.css";
import { Component } from "react";
import Cookies from "js-cookie";
import { Link, withRouter } from "react-router-dom";
import { backend_URL } from "../../constants";
import {
  _email_validator,
  _password_validator,_validate_secret_key
} from "../../validators/param_validators.js";

class ForgotPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      secret_key_err:"",
      secret_key:"",
      email_err: "",
      password_err: "",
    };
  }

  onChangeEmail = (event) => {
    this.setState({ email_err: "" });
    this.setState({ email: event.target.value });
  };
  onBlurEmail = (event) => {
    this.validateEmail(event.target.value);
  };
  validateEmail = (email) => {
    this.setState({ email_err: _email_validator(email) });
  };

  

  onChangeSecretKey= (event) => {
    this.setState({ secret_key_err: "" });
    this.setState({ secret_key: event.target.value });
  };
  onBlurSecretKey = (event) => {
    this.validateSecretKey(event.target.value);
  };
  validateSecretKey = (secret_key) => {
    this.setState({ secret_key_err: _validate_secret_key(secret_key) });
  };










  onChangePassword = (event) => {
    this.setState({ password_err: "" });
    this.setState({ password: event.target.value });
  };
  onBlurPassword = (event) => {
    this.validatePassword(event.target.value);
  };
  validatePassword = (password) => {
    this.setState({ password_err: _password_validator(password) });
  };

  resetErrors = () => {
    this.setState({ email_err: "", password_err: "",secret_key_err:"" });
  };

  submitForm = async (event) => {
    event.preventDefault();

    this.resetErrors();
    this.validateEmail(this.state.email);
    this.validatePassword(this.state.password);
    this.validateSecretKey(this.state.secret_key);

    if (
      _email_validator(this.state.email).length > 0 ||
      _password_validator(this.state.password).length > 0 ||
      _validate_secret_key(this.state.secret_key).length>0
    ) {
      return;
    } else {
      const { email, password, secret_key } = this.state;
      const userDetails = { email, password, secret_key };
      const url = backend_URL + "/forgot";
      const options = {
        method: "PUT", 
        body: JSON.stringify(userDetails),
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
        },
      };

      try {
        const response = await fetch(url, options);
        console.log(response.status);
        const data = await response.json();

        if (response.status === 200) {
          if (data.jwt_token) {
            Cookies.set("jwt_token", data.jwt_token, {
              expires: 30,
            });
            const { history } = this.props;
            history.replace("/profile");
          } else {
            // no jwt token recieved
            this.setState({ password_err: "No JWT Token recieved" });
          }
        } else {
          const err_msg=response.status_message
          this.setState({ password_err:err_msg });
        }
      } catch (error) {
        // unknown error
        this.setState({ password_err: "Unable to contact server." });
      }
    }
  };

  render() {
    const {email,password,secret_key}=this.state

    return (
      <div className="login-root">
        <div
          className="box-root flex-flex flex-direction--column"
          style={{ minHeight: "100vh", flexGrow: 1 }}
        >
          <div className="loginbackground box-background--white padding-top--64">
            <div className="loginbackground-gridContainer">
              <div
                className="box-root flex-flex"
                style={{ gridArea: "top / start / 8 / end" }}
              >
                <div
                  className="box-root"
                  style={{
                    backgroundImage:
                      "linear-gradient(white 0%, rgb(247, 250, 252) 33%)",
                    flexGrow: 1,
                  }}
                ></div>
              </div>
              <div
                className="box-root flex-flex"
                style={{ gridArea: "4 / 2 / auto / 5" }}
              >
                <div
                  className="box-root box-divider--light-all-2 animationLeftRight tans3s"
                  style={{ flexGrow: 1 }}
                />
              </div>
              <div
                className="box-root flex-flex"
                style={{ gridArea: "6 / start / auto / 2" }}
              >
                <div
                  className="box-root box-background--blue800"
                  style={{ flexGrow: 1 }}
                />
              </div>
              <div
                className="box-root flex-flex"
                style={{ gridArea: "7 / start / auto / 4" }}
              >
                <div
                  className="box-root box-background--blue animationLeftRight"
                  style={{ flexGrow: 1 }}
                />
              </div>
              <div
                className="box-root flex-flex"
                style={{ gridArea: "8 / 4 / auto / 6" }}
              >
                <div
                  className="box-root box-background--gray100 animationLeftRight tans3s"
                  style={{ flexGrow: 1 }}
                />
              </div>
              <div
                className="box-root flex-flex"
                style={{ gridArea: "2 / 15 / auto / end" }}
              >
                <div
                  className="box-root box-background--cyan200 animationRightLeft tans4s"
                  style={{ flexGrow: 1 }}
                />
              </div>
              <div
                className="box-root flex-flex"
                style={{ gridArea: "3 / 14 / auto / end" }}
              >
                <div
                  className="box-root box-background--blue animationRightLeft"
                  style={{ flexGrow: 1 }}
                />
              </div>
              <div
                className="box-root flex-flex"
                style={{ gridArea: "4 / 17 / auto / 20" }}
              >
                <div
                  className="box-root box-background--gray100 animationRightLeft tans4s"
                  style={{ flexGrow: 1 }}
                />
              </div>
              <div
                className="box-root flex-flex"
                style={{ gridArea: "5 / 14 / auto / 17" }}
              >
                <div
                  className="box-root box-divider--light-all-2 animationRightLeft tans3s"
                  style={{ flexGrow: 1 }}
                />
              </div>
            </div>
          </div>
          <div
            className="box-root padding-top--24 flex-flex flex-direction--column"
            style={{ flexGrow: 1, zIndex: 9 }}
          >
            <h1 className="login">Work-Space</h1>
            <div className="formbg-outer">
              <div className="formbg">
                <div className="formbg-inner padding-horizontal--48">
                  <span className="padding-bottom--15">Forgot Password</span>
                  <form id="stripe-login">
                    <div className="field padding-bottom--24">
                      <label htmlFor="email">Email</label>
                      <input type="email" name="email" onChange={this.onChangeEmail} onBlur={this.onBlurEmail} value={email}/>
                      <p
                        style={{
                          color: "red",
                          fontSize: "x-small",
                          textAlign: "center",
                        }}
                      >
                        {this.state.email_err}
                      </p>
                    </div>
                    <div className="field padding-bottom--24">
                      <label htmlFor="secret_key">Secret key</label>

                      <input type="text" name="secret_key" onChange={this.onChangeSecretKey} onBlur={this.onBlurSecretKey} value={secret_key}/>
                      <p
                        style={{
                          color: "red",
                          fontSize: "x-small",
                          textAlign: "center",
                        }}
                      >
                        {this.state.secret_key_err}
                      </p>
                    </div>
                    <div className="field padding-bottom--24">
                      <label htmlFor="password"> New Password</label>

                      <input type="password" name="password" onChange={this.onChangePassword} onBlur={this.onBlurPassword} value={password}/>
                      <p
                        style={{
                          color: "red",
                          fontSize: "x-small",
                          textAlign: "center",
                        }}
                      >
                        {this.state.password_err}
                      </p>
                    </div>
                    <Link to="/login">
                    <div className="field padding-bottom--24">
                      <input
                        type="Submit"
                        name="Back"
                        defaultValue="Back" 
                      />
    
                    </div>
                    </Link>
                    <div className="field padding-bottom--24">
                      <input
                        type="submit"
                        name="submit"
                        defaultValue="Update Password" onClick={this.submitForm}
                      />
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(ForgotPage);
