import "./employmonthly.css";
import React, { Component } from 'react';

class Employmonthly extends Component {
    render() {
        return (
            <>
  <div className="top_bar">
    <div className="d-flex flex-row">
      <div className="top_bar_data">
        <h1 className="cmp_name">Company</h1>
        <p className="cmp_detail">Work Space</p>
      </div>
      <div className="top_bar_icon">
        <div className="d-flex flex-row">
          <div>
            <p className="top_bar_quote">
              Success isn't always about greatness. <br />
              It's about consistency. Consistent hard work leads to success.
              Greatness will come.
            </p>
          </div>
          <div>
            <img
              src="https://cdn.pixabay.com/photo/2018/03/10/12/00/teamwork-3213924__340.jpg"
              className="top_bar_image"
            />
          </div>
        </div>
      </div>
    </div>
  </div>
  <div className="d-flex flex-row">
    <div className="side_bar">
      <div className="profile_data">
        <img
          src="https://1.bp.blogspot.com/-Muc21xaQjYg/XluhYO6f6vI/AAAAAAAAPJw/T51WnNCctz4Vxn_9REzJuftkznfuRGKUgCLcBGAsYHQ/s1600/whatsapp%2Bprofile%2Bpic%2B%25281%2529.jpg"
          className="profile"
        />
        <h1 className="profile_name">Harish Kumar</h1>
      </div>
      <ul>
              <li>
                <i class="fas fa-user-alt"></i>Profile
              </li>
              <li className="hr_active">
                <i className="fas fa-business-time" />
                Time Sheet
              </li>
              <li>
                <i className="fas fa-envelope-open-text" />
                Time Sheet Requests
              </li>
              <li>
                <i className="fas fa-address-card" /> Create Profile{" "}
              </li>
              <li>
                <i className="fas fa-user-alt-slash" /> Deactivate Profile{" "}
              </li>
              <li>
                <i className="fas fa-sign-out-alt" /> Log Out{" "}
              </li>
            
      </ul>
    </div>
    <div className="main_sec">
      <h1 className="welcome_msg"> Welcome, Harish Kumar </h1>
      <p className="ts_date"> Monthly Summary </p>
      <table className="ts_table">
        <tbody>
          <tr className="ts_row">
            <th className="ts_head">Date</th>
            <th className="ts_head">Project</th>
            <th className="ts_head">Hours</th>
            <th className="ts_head">Status</th>
          </tr>
          <tr className="ts_row">
            <td className="ts_col">01-Feb-2022</td>
            <td className="ts_col">Organizaton Tasks</td>
            <td className="ts_col">9</td>
            <td className="ts_col">Approved</td>
          </tr>
          <tr className="ts_row">
            <td className="ts_col">02-Feb-2022</td>
            <td className="ts_col">Organizaton Tasks</td>
            <td className="ts_col">9</td>
            <td className="ts_col">Approved</td>
          </tr>
          <tr className="ts_row">
            <td className="ts_col">03-Feb-2022</td>
            <td className="ts_col">Organizaton Tasks</td>
            <td className="ts_col">9</td>
            <td className="ts_col">Approved</td>
          </tr>
          <tr className="ts_row">
            <td className="ts_col">04-Feb-2022</td>
            <td className="ts_col">Organizaton Tasks</td>
            <td className="ts_col">9</td>
            <td className="ts_col">Approved</td>
          </tr>
          <tr className="ts_row">
            <td className="ts_col">05-Feb-2022</td>
            <td className="ts_col">Organizaton Tasks</td>
            <td className="ts_col">9</td>
            <td className="ts_col">Approved</td>
          </tr>
          <tr className="ts_row">
            <td className="ts_col">06-Feb-2022</td>
            <td className="ts_col">Organizaton Tasks</td>
            <td className="ts_col">9</td>
            <td className="ts_col">Approved</td>
          </tr>
          <tr className="ts_row">
            <td className="ts_col">07-Feb-2022</td>
            <td className="ts_col">Organizaton Tasks</td>
            <td className="ts_col">7</td>
            <td className="ts_col">Rejected</td>
          </tr>
          <tr className="ts_row">
            <td className="ts_col">08-Feb-2022</td>
            <td className="ts_col">Organizaton Tasks</td>
            <td className="ts_col">9</td>
            <td className="ts_col">Approved</td>
          </tr>
          <tr className="ts_row">
            <td className="ts_col">09-Feb-2022</td>
            <td className="ts_col">Organizaton Tasks</td>
            <td className="ts_col">9</td>
            <td className="ts_col">Approved</td>
          </tr>
          <tr className="ts_row">
            <td className="ts_col">10-Feb-2022</td>
            <td className="ts_col">Organizaton Tasks</td>
            <td className="ts_col">9</td>
            <td className="ts_col">Approved</td>
          </tr>
        </tbody>
      </table>
      <button className="emp_ts_button"> Back </button>
      <button className="emp_ts_button"> Download Excel </button>
    </div>
  </div>
  <div className="bottom_bar">
    <h1 className="bottom_bar_heading">Company</h1>
    <p className="bottom_bar_desc">
      {" "}
      Copyright @2022, Company Software Pvt Ltd. All rights reserved.{" "}
    </p>
  </div>
</>

        );
    }
}

export default Employmonthly;