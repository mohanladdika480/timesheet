import "./HrApprovalPage.css"

import React, { Component } from 'react';

class HrApprovalPage extends Component {
    render() {
        return (
            <>
            <div className="hr_top_bar">
              <div className="d-flex flex-row">
                <div className="hr_top_bar_data">
                  <h1 className="hr_cmp_name">Company</h1>
                  <p className="hr_cmp_detail">Work Space</p>
                </div>
                <div className="hr_top_bar_icon">
                  <div className="d-flex flex-row">
                    <div>
                      <p className="hr_top_bar_quote">
                        Success isn't always about greatness. <br />
                        It's about consistency. Consistent hard work leads to success.
                        Greatness will come.
                      </p>
                    </div>
                    <div>
                      <img
                        src="https://cdn.pixabay.com/photo/2018/03/10/12/00/teamwork-3213924__340.jpg"
                        className="hr_top_bar_image"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="d-flex flex-row">
              <div className="hr_side_bar">
                <div className="hr_profile_data">
                  <img
                    src="https://1.bp.blogspot.com/-Muc21xaQjYg/XluhYO6f6vI/AAAAAAAAPJw/T51WnNCctz4Vxn_9REzJuftkznfuRGKUgCLcBGAsYHQ/s1600/whatsapp%2Bprofile%2Bpic%2B%25281%2529.jpg"
                    className="hr_profile"
                  />
                  <h1 className="hr_profile_name">Harish Kumar</h1>
                </div>
                <ul>
              <li>
                <i class="fas fa-user-alt"></i>Profile
              </li>
              <li>
                <i className="fas fa-business-time" />
                Time Sheet
              </li>
              <li className="hr_active">
                <i className="fas fa-envelope-open-text" />
                Time Sheet Requests
              </li>
              <li>
                <i className="fas fa-address-card" /> Create Profile{" "}
              </li>
              <li>
                <i className="fas fa-user-alt-slash" /> Deactivate Profile{" "}
              </li>
              <li>
                <i className="fas fa-sign-out-alt" /> Log Out{" "}
              </li>
            </ul>
              </div>
              <div className="hr_main_sec">
                <h1 className="hr_emp_msg"> 10737 -Praneetha- TimeSheet</h1>
                <div className="d-flex flex-column">
                  <div className="hr_approval_block">
                    <p className="approval_content">
                      26-02-22 - Organisation tasks - 8 hours
                    </p>
                    <div className="d-flex">
                      <button className="hr_reject_button"> Reject </button>
                      <button className="hr_approve_button"> Approve </button>
                    </div>
                  </div>
                  <div className="hr_approval_block">
                    <p className="approval_content">
                      26-02-22 - Organisation tasks - 8 hours
                    </p>
                    <div className="d-flex">
                      <button className="hr_reject_button"> Reject </button>
                      <button className="hr_approve_button"> Approve </button>
                    </div>
                  </div>
                  <div className="hr_approval_block">
                    <p className="approval_content">
                      26-02-22 - Organisation tasks - 8 hours
                    </p>
                    <div className="d-flex">
                      <button className="hr_reject_button"> Reject </button>
                      <button className="hr_approve_button"> Approve </button>
                    </div>
                  </div>
                  <div className="hr_approval_block">
                    <p className="approval_content">
                      26-02-22 - Organisation tasks - 8 hours
                    </p>
                    <div className="d-flex">
                      <button className="hr_reject_button"> Reject </button>
                      <button className="hr_approve_button"> Approve </button>
                    </div>
                  </div>
                  <div className="hr_approval_block">
                    <p className="approval_content">
                      26-02-22 - Organisation tasks - 8 hours
                    </p>
                    <div className="d-flex">
                      <button className="hr_reject_button"> Reject </button>
                      <button className="hr_approve_button"> Approve </button>
                    </div>
                  </div>
                  <button className="hr_emp_button"> Back </button>
                </div>
              </div>
            </div>
            <div className="hr_bottom_bar">
              <h1 className="hr_bottom_bar_heading">Company</h1>
              <p className="hr_bottom_bar_desc">
                {" "}
                Copyright @2022, Company Software Pvt Ltd. All rights reserved.{" "}
              </p>
            </div>
          </>
          
        );
    }
}

export default HrApprovalPage;