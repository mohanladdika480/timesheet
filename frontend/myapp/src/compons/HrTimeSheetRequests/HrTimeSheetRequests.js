import React, { Component } from "react";

class HrTimeSheetRequests extends Component {
  render() {
    return (
      <>
        <div className="hr_top_bar">
          <div className="d-flex flex-row">
            <div className="hr_top_bar_data">
              <h1 className="hr_cmp_name">Company</h1>
              <p className="hr_cmp_detail">Work Space</p>
            </div>
            <div className="hr_top_bar_icon">
              <div className="d-flex flex-row">
                <div>
                  <p className="hr_top_bar_quote">
                    Success isn't always about greatness. <br />
                    It's about consistency. Consistent hard work leads to
                    success. Greatness will come.
                  </p>
                </div>
                <div>
                  <img
                    src="https://cdn.pixabay.com/photo/2018/03/10/12/00/teamwork-3213924__340.jpg"
                    className="hr_top_bar_image"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="d-flex flex-row">
          <div className="hr_side_bar">
            <div className="hr_profile_data">
              <img
                src="https://1.bp.blogspot.com/-Muc21xaQjYg/XluhYO6f6vI/AAAAAAAAPJw/T51WnNCctz4Vxn_9REzJuftkznfuRGKUgCLcBGAsYHQ/s1600/whatsapp%2Bprofile%2Bpic%2B%25281%2529.jpg"
                className="hr_profile"
              />
              <h1 className="hr_profile_name">Harish Kumar</h1>
            </div>
            <ul>
              <li>
                <i class="fas fa-user-alt"></i>Profile
              </li>
              <li>
                <i className="fas fa-business-time" />
                Time Sheet
              </li>
              <li className="hr_active">
                <i className="fas fa-envelope-open-text" />
                Time Sheet Requests
              </li>
              <li>
                <i className="fas fa-address-card" /> Create Profile{" "}
              </li>
              <li>
                <i className="fas fa-user-alt-slash" /> Deactivate Profile{" "}
              </li>
              <li>
                <i className="fas fa-sign-out-alt" /> Log Out{" "}
              </li>
            </ul>
          </div>
          <div className="hr_main_sec">
            <h1 className="hr_welcome_msg"> Welcome, HR</h1>
            <p className="hr_ts_date"> TimeSheets Requests</p>
            <table className="hr_ts_table">
              <tbody>
                <tr className="hr_ts_row">
                  <th className="hr_ts_head">Employee Id</th>
                  <th className="hr_ts_head">Name</th>
                  <th className="hr_ts_head">Action</th>
                </tr>
                <tr className="hr_ts_row">
                  <td className="hr_ts_col">10737</td>
                  <td className="hr_ts_col">Saanvi</td>
                  <td className="hr_ts_col">
                    {" "}
                    <button className="hr_emp_ts_button"> Open </button>{" "}
                  </td>
                </tr>
                <tr className="hr_ts_row">
                  <td className="hr_ts_col">10735</td>
                  <td className="hr_ts_col">Purushottam</td>
                  <td className="hr_ts_col">
                    {" "}
                    <button className="hr_emp_ts_button"> Open </button>{" "}
                  </td>
                </tr>
                <tr className="hr_ts_row">
                  <td className="hr_ts_col">10736</td>
                  <td className="hr_ts_col">Varma</td>
                  <td className="hr_ts_col">
                    {" "}
                    <button className="hr_emp_ts_button"> Open </button>
                  </td>
                </tr>
                <tr className="hr_ts_row">
                  <td className="hr_ts_col">10734</td>
                  <td className="hr_ts_col">Bharath</td>
                  <td className="hr_ts_col">
                    {" "}
                    <button className="hr_emp_ts_button"> Open </button>
                  </td>
                </tr>
                <tr className="hr_ts_row">
                  <td className="hr_ts_col">10732</td>
                  <td className="hr_ts_col">Praneetha</td>
                  <td className="hr_ts_col">
                    {" "}
                    <button className="hr_emp_ts_button"> Open </button>
                  </td>
                </tr>
              </tbody>
            </table>
            <button className="hr_emp_button"> Back </button>
          </div>
        </div>
        <div className="hr_bottom_bar">
          <h1 className="hr_bottom_bar_heading">Company</h1>
          <p className="hr_bottom_bar_desc">
            {" "}
            Copyright @2022, Company Software Pvt Ltd. All rights reserved.{" "}
          </p>
        </div>
      </>
    );
  }
}

export default HrTimeSheetRequests;
