import React, { Component } from "react";
import "./createprofile.css";
import Cookies from "js-cookie"
import { backend_URL } from "../../constants";
import { Link, withRouter } from "react-router-dom";

class Createprofile extends Component {

  constructor(props) {
    super(props);
    this.state = {
			email: "",
			password: "",
			mobile_no: "",
			aadhar_no: "",
      emp_id: "",
      role: "",
      password: "",
      dob: "",
      gender: "",
      date_of_join: "",
      secret_key: "",
      emp_status: "",
      name: ""
		};
  }

  onChangeName = (event) => {
    this.setState({name:event.target.value})
  }

  onChangeAadhar = (event) => {
    this.setState({aadhar_no:event.target.value})
  }

  onChangeDOB = (event) => {
    this.setState({dob: event.target.value})
  }

  onChangeDateOfJoin = (event) => {
    this.setState({date_of_join: event.target.value})
  }

  onChangeGender = (event) => {
    this.setState({gender: event.target.value})
  }

  onChangeMail = (event) => {
    this.setState({email: event.target.value})
  }

  onChangeMobile = (event) => {
    this.setState({mobile_no: event.target.value})
  }

  onChangeStatus = (event) => {
    this.setState({emp_status: event.target.value})
  }

  onChangeRole = (event) => {
    this.setState({role: event.target.value})
  }

  onChangePassword = (event) => {
    this.setState({password: event.target.value})
  }

  onChangeSecret =(event) => {
    this.setState({secret_key: event.target.value})
  }

  onChangeEmployee = (event) => {
    this.setState({emp_id: event.target.value})
  }

  submitForm = async (event) => {
    event.preventDefault()
    const {name, email, gender, role, emp_id, password, date_of_join, dob, emp_status, aadhar_no, mobile_no, secret_key} = this.state

    const userDetails = {name, email, gender, role, emp_id, password, date_of_join, dob, emp_status, aadhar_no, mobile_no, secret_key}

    const url = backend_URL + "/register"
    const token=Cookies.get("jwt_token")

    const options = {
      method: "POST",
      body: JSON.stringify(userDetails),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization':'Bearer '+token
      },
    };

    try {
      const response = await fetch(url, options);
      console.log(response.status)
      const data = await response.json();

      if (response.status === 201) {
        const msg = "Profile Created Successfully"
        alert(msg)
      }
      else {
        const msg = "Profile Not created"
        alert(msg)
      }
    }
    catch {
      const msg = "unable to connect the server"
      alert(msg)
    }


  }



  render() {
    const {name, email, gender, role, emp_id, password, date_of_join, dob, emp_status, aadhar_no, mobile_no, secret_key} = this.state
    return (
      <>
        <div className="top_bar">
          <div className="d-flex flex-row">
            <div className="top_bar_data">
              <h1 className="cmp_name">Company</h1>
              <p className="cmp_detail">Work Space</p>
            </div>
            <div className="top_bar_icon">
              <div className="d-flex flex-row">
                <div>
                  <p className="top_bar_quote">
                    Success isn't always about greatness. <br />
                    It's about consistency. Consistent hard work leads to
                    success. Greatness will come.
                  </p>
                </div>
                <div>
                  <img
                    src="https://cdn.pixabay.com/photo/2018/03/10/12/00/teamwork-3213924__340.jpg"
                    className="top_bar_image"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="d-flex flex-row">
          <div className="side_bar">
            <div className="profile_data">
              <img
                src="https://1.bp.blogspot.com/-Muc21xaQjYg/XluhYO6f6vI/AAAAAAAAPJw/T51WnNCctz4Vxn_9REzJuftkznfuRGKUgCLcBGAsYHQ/s1600/whatsapp%2Bprofile%2Bpic%2B%25281%2529.jpg"
                className="profile"
              />
              <h1 className="profile_name">Harish Kumar</h1>
            </div>
            <ul>
              <li>
                <Link to="/profile">
                <i className="fas fa-user-alt"></i>Profile
                </Link>
              </li>
              <li>
                <Link to="/timesheet">
                <i className="fas fa-business-time" />
                Time Sheet
                </Link>
              </li>
              
              <li className="hr_active">
                <i className="fas fa-address-card" /> Create Profile{" "}
              </li>
              <li>
                <i className="fas fa-user-alt-slash" /> Deactivate Profile{" "}
              </li>
              <li>
                <i className="fas fa-sign-out-alt" /> Log Out{" "}
              </li>
            </ul>
          </div>
          <div className="main_sec">
            <h1 className="welcome_msg"> Welcome, Harish Kumar </h1>
            <h1 className="admin_emp_create">New Employee Profile </h1>
            <form className="emp_form">
              <label htmlFor="name" className="label_emp">
                {" "}
                Employee Name{" "}
              </label>
              <input type="text" name="name" onChange={this.onChangeName} value={name}/>
              <br />
              <label htmlFor="emp_mail" className="label_emp">
                {" "}
                Mail
              </label>
              <input type="text" name="emp_mail"  onChange={this.onChangeMail} value={email}/>
              <br />
              <label htmlFor="emp_mobile_no" className="label_emp">
                {" "}
                Mobile Number
              </label>
              <input type="text" name="emp_mobile_no" maxLength={10}  onChange={this.onChangeMobile} value={mobile_no}/>
              <br />
              <label htmlFor="emp_dob" className="label_emp">
                {" "}
                Date of Birth
              </label>
              <input type="date" name="emp_dob"  onChange={this.onChangeDOB} value={dob}/>
              <br />
              <label htmlFor="emp_gender" className="label_emp">
                {" "}
                Gender{" "}
              </label>
              <input list="gender" name="emp_gender"  onChange={this.onChangeGender} value={gender}/>
              <datalist id="gender">
                <option value="Male"></option>
                <option value="Female"></option>
                <option value="Others"></option>
              </datalist>
              <br />
              <label htmlFor="emp_aadhar" className="label_emp">
                {" "}
                Aadhar Number{" "}
              </label>
              <input type="text" name="emp_aadhar"  onChange={this.onChangeAadhar} value={aadhar_no}/>
              <label htmlFor="emp_password" className="label_emp">
                {" "}
                Password
              </label>
              <input type="password" name="emp_password" maxLength={12}  onChange={this.onChangePassword} value={password}/>
              <br />
              <label htmlFor="emp_id" className="label_emp">
                {" "}
                Employee Id
              </label>
              <input type="text" name="emp_id" maxLength={12}  onChange={this.onChangeEmployee} value={emp_id}/>
              <br />
              <label htmlFor="emp_role" className="label_emp">
                {" "}
                Role{" "}
              </label>
              <input list="role" name="emp_gender"  onChange={this.onChangeRole} value={role}/>
              <datalist id="role">
                <option value="Employee"></option>
                <option value="Admin"></option>
                <option value="HR"></option>
              </datalist>
              <br />
              <label htmlFor="emp_status" className="label_emp">
                {" "}
                Status{" "}
              </label>
              <input list="status" name="emp_status"  onChange={this.onChangeStatus} value={emp_status}/>
              <datalist id="status">
                <option value="Active"></option>
                <option value="Deactivated"></option>
              </datalist>
              <br />
              <label htmlFor="emp_date_of_join" className="label_emp">
                {" "}
                Date of Join
              </label>
              <input type="date" name="emp_date_of_join"  onChange={this.onChangeDateOfJoin} value={date_of_join}/>
              <br />
              <label htmlFor="emp_secret_key" className="label_emp">
                {" "}
                Secret Key
              </label>
              <input type="text" name="emp_secret_key" maxLength={30}  onChange={this.onChangeSecret} value={secret_key}/>
            </form>
            <button className="emp_ts_button" onClick={this.submitForm}> Create Profile </button>
          </div>
        </div>
        <div className="bottom_bar">
          <h1 className="bottom_bar_heading">Company</h1>
          <p className="bottom_bar_desc">
            {" "}
            Copyright @2022, Company Software Pvt Ltd. All rights reserved.{" "}
          </p>
        </div>
      </>
    );
  }
}

export default withRouter(Createprofile);
