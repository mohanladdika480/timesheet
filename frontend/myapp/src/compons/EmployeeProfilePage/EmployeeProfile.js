import React, { Component } from "react";
import "./EmployeeProfile.css";
import Cookies from "js-cookie"
import { Link, withRouter } from "react-router-dom";
import { backend_URL } from "../../constants";

class EmployeeProfile extends Component {

  
  LogOutSession=()=>{
    const {history} = this.props
    Cookies.remove("jwt_token")
    const token = Cookies.get("jwt_token")
    if (typeof(token) === "undefined") {
      history.replace("/login")
    }
    
  }

  state = {
    Employee_data : {}
}

componentDidMount(){
    this.getServerData()
}

getServerData = async()=>{
  const token = Cookies.get("jwt_token")
    const url = backend_URL + "/employee_data"
    const option = {
        method:"GET",
        headers:{
            "Content-Type":"application/json",
            "Accept":"application/json",
            "Authorization" : "Bearer " + token
        }
    }
    const response = await fetch(url,option)
    
    const data = await response.json()

    const role = data.role

    let display_utility_admin =""

    if (role === "Admin") {
      display_utility_admin =  "display"
    }
    else {
      display_utility_admin = "hide"
    }

    data.display_utility_admin = display_utility_admin
    
    let display_utility_hr = ""

    if (role == "HR") {
      display_utility_hr = "display"
    }
    else {
      display_utility_hr = "hide"
    }
    data.display_utility_hr = display_utility_hr

    this.setState({
      
        Employee_data:data
    })
    
}

  render() {
     const {name, aadhar_no, mobile_no, role, email, emp_id, dob, date_of_join, display_utility_admin, display_utility_hr} = this.state.Employee_data
     
    return (
      <>
        <div className="top_bar">
          <div className="d-flex flex-row">
            <div className="top_bar_data">
              <h1 className="cmp_name">Company</h1>
              <p className="cmp_detail">Work Space</p>
            </div>
            <div className="top_bar_icon">
              <div className="d-flex flex-row">
                <div>
                  <p className="top_bar_quote">
                    Success isn't always about greatness. <br />
                    It's about consistency. Consistent hard work leads to
                    success. Greatness will come.
                  </p>
                </div>
                <div>
                  <img
                    src="https://cdn.pixabay.com/photo/2018/03/10/12/00/teamwork-3213924__340.jpg"
                    className="top_bar_image"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="d-flex flex-row">
          <div className="side_bar">
            <div className="profile_data">
              <img
                src="https://1.bp.blogspot.com/-Muc21xaQjYg/XluhYO6f6vI/AAAAAAAAPJw/T51WnNCctz4Vxn_9REzJuftkznfuRGKUgCLcBGAsYHQ/s1600/whatsapp%2Bprofile%2Bpic%2B%25281%2529.jpg"
                className="profile"
              />
              <h1 className="profile_name">{name}</h1>
            </div>
            <ul>
              <li className="hr_active">
                <i className="fas fa-user-alt"></i>Profile
              </li>
              <li>
                <Link to="/timesheet">
                <i className="fas fa-business-time" />
                Time Sheet
                </Link>
              </li>
              <li className={display_utility_hr}>
                <Link to="/hr_ts_approval">
                <i className="fas fa-envelope-open-text" />
                Time Sheet Requests
                </Link>
              </li>

              <li className={display_utility_admin}>
                <Link to="/create_profile">
                <i className="fas fa-address-card" /> Create Profile{" "}
                </Link>
              </li>

              <li className={display_utility_admin}>
                <Link to="/deactivate_employee">
                <i className="fas fa-user-alt-slash" /> Deactivate Profile{" "}
                </Link>
              </li>
              <li onClick={this.LogOutSession}>
             
                <i className="fas fa-sign-out-alt" /> Log Out{" "}
       
              </li>

            </ul>
          </div>
          <div className="main_sec">
            <h1 className="welcome_msg"> Welcome, {name} </h1>
            <table className="ts_table_emp">
              <tbody>
                <tr className="ts_row">
                  <td className="ts_col_name"> Name </td>
                  <td className="ts_col_value"> {name} </td>
                </tr>
                <tr className="ts_row">
                  <td className="ts_col_name"> Employee Id </td>
                  <td className="ts_col_value"> {emp_id} </td>
                </tr>
                <tr className="ts_row">
                  <td className="ts_col_name"> Email </td>
                  <td className="ts_col_value">{email}</td>
                </tr>
                <tr className="ts_row">
                  <td className="ts_col_name"> Date of Birth </td>
                  <td className="ts_col_value"> {dob} </td>
                </tr>
                <tr className="ts_row">
                  <td className="ts_col_name"> Role </td>
                  <td className="ts_col_value"> {role} </td>
                </tr>
                <tr className="ts_row">
                  <td className="ts_col_name"> Mobile Number </td>
                  <td className="ts_col_value"> {mobile_no} </td>
                </tr>
                <tr className="ts_row">
                  <td className="ts_col_name"> Aadhar Number </td>
                  <td className="ts_col_value"> {aadhar_no} </td>
                </tr>
                <tr className="ts_row">
                  <td className="ts_col_name"> Date of Join </td>
                  <td className="ts_col_value"> {date_of_join} </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div className="bottom_bar">
          <h1 className="bottom_bar_heading">Company</h1>
          <p className="bottom_bar_desc">
            {" "}
            Copyright @2022, Company Software Pvt Ltd. All rights reserved.{" "}
          </p>
        </div>
      </>
    );
  }
}


export default withRouter(EmployeeProfile);
