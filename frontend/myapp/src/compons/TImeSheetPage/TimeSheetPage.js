import "./TimesheetPage.css";
import React, { Component } from "react";

class HrTimeSheetPage extends Component {
  render() {
    return (
      <>
        <div className="top_bar">
          <div className="d-flex flex-row">
            <div className="top_bar_data">
              <h1 className="cmp_name">Company</h1>
              <p className="cmp_detail">Work Space</p>
            </div>
            <div className="top_bar_icon">
              <div className="d-flex flex-row">
                <div>
                  <p className="top_bar_quote">
                    Success isn't always about greatness. <br />
                    It's about consistency. Consistent hard work leads to
                    success. Greatness will come.
                  </p>
                </div>
                <div>
                  <img
                    src="https://cdn.pixabay.com/photo/2018/03/10/12/00/teamwork-3213924__340.jpg"
                    className="top_bar_image"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="d-flex flex-row">
          <div className="side_bar">
            <div className="profile_data">
              <img
                src="https://1.bp.blogspot.com/-Muc21xaQjYg/XluhYO6f6vI/AAAAAAAAPJw/T51WnNCctz4Vxn_9REzJuftkznfuRGKUgCLcBGAsYHQ/s1600/whatsapp%2Bprofile%2Bpic%2B%25281%2529.jpg"
                className="profile"
              />
              <h1 className="profile_name">Harish Kumar</h1>
            </div>
            <ul>
              <li>
                <i class="fas fa-user-alt"></i>Profile
              </li>
              <li className="hr_active">
                <i className="fas fa-business-time" />
                Time Sheet
              </li>
              <li>
                <i className="fas fa-envelope-open-text" />
                Time Sheet Requests
              </li>
              <li>
                <i className="fas fa-address-card" /> Create Profile{" "}
              </li>
              <li>
                <i className="fas fa-user-alt-slash" /> Deactivate Profile{" "}
              </li>
              <li>
                <i className="fas fa-sign-out-alt" /> Log Out{" "}
              </li>
            </ul>
          </div>
          <div className="main_sec">
            <h1 className="welcome_msg"> Welcome, Harish Kumar </h1>
            <p className="ts_date"> Date: 26-February-2022</p>
            <table className="ts_table">
              <tbody>
                <tr className="ts_row">
                  <th className="ts_head">Task</th>
                  <th className="ts_head">Hours</th>
                  <th className="ts_head">Description</th>
                </tr>
                <tr className="ts_row">
                  <td className="ts_col">Meetings</td>
                  <td className="ts_col">
                    <input type="text" maxLength={2} placeholder="hours" />{" "}
                  </td>
                  <td className="ts_col">
                    <input type="text" placeholder="description" />{" "}
                  </td>
                </tr>
                <tr className="ts_row">
                  <td className="ts_col">Training</td>
                  <td className="ts_col">
                    <input type="text" maxLength={2} placeholder="hours" />{" "}
                  </td>
                  <td className="ts_col">
                    <input type="text" placeholder="description" />{" "}
                  </td>
                </tr>
                <tr className="ts_row">
                  <td className="ts_col">Appraisals</td>
                  <td className="ts_col">
                    <input type="text" maxLength={2} placeholder="hours" />{" "}
                  </td>
                  <td className="ts_col">
                    <input type="text" placeholder="description" />{" "}
                  </td>
                </tr>
                <tr className="ts_row">
                  <td className="ts_col">Orgonization Tasks</td>
                  <td className="ts_col">
                    <input type="text" maxLength={2} placeholder="hours" />{" "}
                  </td>
                  <td className="ts_col">
                    <input type="text" placeholder="description" />{" "}
                  </td>
                </tr>
                <tr className="ts_row">
                  <td className="ts_col">Hiring and Interviewing</td>
                  <td className="ts_col">
                    <input type="text" maxLength={2} placeholder="hours" />{" "}
                  </td>
                  <td className="ts_col">
                    <input type="text" placeholder="description" />{" "}
                  </td>
                </tr>
              </tbody>
            </table>
            <button className="emp_ts_button"> Submit </button>
            <button className="emp_ts_button"> Monthly Summary </button>
          </div>
        </div>
        <div className="bottom_bar">
          <h1 className="bottom_bar_heading">Company</h1>
          <p className="bottom_bar_desc">
            {" "}
            Copyright @2022, Company Software Pvt Ltd. All rights reserved.{" "}
          </p>
        </div>
      </>
    );
  }
}

export default HrTimeSheetPage;
